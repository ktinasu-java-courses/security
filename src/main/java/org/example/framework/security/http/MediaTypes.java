package org.example.framework.security.http;

public enum MediaTypes {
  ANY("*/*"),
  APPLICATION_JSON("application/json"),
  TEXT_PLAIN("text/plain");

  private final String value;

  MediaTypes(String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }

  public static MediaTypes fromValue(final String value) {
    for (MediaTypes mediaType : MediaTypes.values()) {
      if (mediaType.value.equals(value)) {
        return mediaType;
      }
    }

    throw new IllegalArgumentException(value);
  }
}

