package org.example.framework.security.http;

public enum HttpMethods {
    GET,
    POST,
    PUT,
    PATCH,
    DELETE;
}

