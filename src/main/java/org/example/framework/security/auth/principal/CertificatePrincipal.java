package org.example.framework.security.auth.principal;

import lombok.RequiredArgsConstructor;

import java.security.Principal;

@RequiredArgsConstructor
public class CertificatePrincipal implements Principal {
    private final String commonName;

    @Override
    public String getName() {
        return commonName;
    }
}

