package org.example.framework.security.auth.principal;

import java.security.Principal;

public class AnonymousPrincipal implements Principal {
    public static final String ANONYMOUS = "ANONYMOUS";

    @Override
    public String getName() {
        return ANONYMOUS;
    }
}
