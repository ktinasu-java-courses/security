package org.example.framework.security.auth;

public interface AuthenticationToken {
    Object getLogin();

    Object getCredentials();
}

