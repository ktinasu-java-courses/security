package org.example.framework.security.auth;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class LoginPasswordAuthenticationToken implements AuthenticationToken {
    @Getter
    private final String login;
    private final String password;

    @Override
    public Object getCredentials() {
        return password;
    }
}

