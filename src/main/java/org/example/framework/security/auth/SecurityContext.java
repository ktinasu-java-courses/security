package org.example.framework.security.auth;

import org.example.framework.security.proxy.Auth;

import java.security.Principal;

public class SecurityContext {
    private static final ThreadLocal<Principal> principalHolder = new ThreadLocal<>();
    private static final ThreadLocal<Auth> authRole = new ThreadLocal<>();

    private SecurityContext() {
    }

    public static void setAuthRole(final Auth auth) {
        authRole.set(auth);
    }

    public static Auth getAuthRole() {
        return authRole.get();
    }

    public static void setPrincipal(final Principal principal) {
        principalHolder.set(principal);
    }

    public static Principal getPrincipal() {
        return principalHolder.get();
    }

    public static void clear() {
        principalHolder.remove();
        authRole.remove();
    }
}

