package org.example.framework.security.middleware.anon;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.auth.principal.AnonymousPrincipal;
import org.example.framework.security.middleware.Middleware;
import org.example.framework.security.request.Request;

import java.net.Socket;

@Slf4j
public class AnonAuthMiddleware implements Middleware {
    @Override
    public void handle(final Socket socket, final Request request) {
        if (SecurityContext.getPrincipal() != null) {
            return;
        }

        SecurityContext.setPrincipal(new AnonymousPrincipal());
    }
}

