package org.example.framework.security.middleware;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.security.exception.AuthenticationException;
import org.example.framework.security.exception.BadAuthenticationException;
import org.example.framework.security.http.HttpHeaders;
import org.example.framework.security.request.Request;

import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@RequiredArgsConstructor
public class BasicAuthNMiddleware implements Middleware {
    private static final String SCHEME = "Basic ";
    private final Authenticator authenticator;

    @Override
    public void handle(final Socket socket, final Request request) {
        if (SecurityContext.getPrincipal() != null) {
            return;
        }

        try {
            final String header = request.getHeaders().get(HttpHeaders.AUTHORIZATION.value());
            if (header == null) {
                return;
            }
            if (!header.startsWith(SCHEME)) {
                return;
            }
            final byte[] decoded = Base64.getDecoder().decode(header.substring(SCHEME.length()));
            final String[] parts = new String(decoded, StandardCharsets.UTF_8).split(":", 2);
            if (parts.length != 2) {
                log.error("invalid authorization header: {}", header);
                throw new BadAuthenticationException("invalid authorization header format");
            }
            final String login = parts[0];
            final String password = parts[1];

            final LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(login, password);
            if (!authenticator.authenticate(authRequest)) {
                throw new AuthenticationException("can't authenticate");
            }
            SecurityContext.setPrincipal(new LoginPrincipal(login));
        } catch (Exception e) {
            SecurityContext.clear();
        }
    }
}

