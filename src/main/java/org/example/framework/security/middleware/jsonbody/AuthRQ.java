package org.example.framework.security.middleware.jsonbody;

import lombok.Data;

@Data
public class AuthRQ {
    private String login;
    private String password;
}

