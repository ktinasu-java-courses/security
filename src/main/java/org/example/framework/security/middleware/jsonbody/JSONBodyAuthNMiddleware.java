package org.example.framework.security.middleware.jsonbody;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.security.exception.AuthenticationException;
import org.example.framework.security.middleware.Middleware;
import org.example.framework.security.proxy.Auth;
import org.example.framework.security.request.Request;

import java.net.Socket;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
public class JSONBodyAuthNMiddleware implements Middleware {
    private final Gson gson;
    private final Authenticator authenticator;

    @Override
    public void handle(final Socket socket, final Request request) {
        if (SecurityContext.getPrincipal() != null) {
            return;
        }
        try {

            final AuthRQ authRQ = gson.fromJson(new String(request.getBody(), StandardCharsets.UTF_8), AuthRQ.class);

            final LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(authRQ.getLogin(), authRQ.getPassword());
            if (!authenticator.authenticate(authRequest)) {
                throw new AuthenticationException("can't authenticate");
            }
            SecurityContext.setPrincipal(new LoginPrincipal(authRQ.getLogin()));
            SecurityContext.setAuthRole(new Auth(authRQ.getLogin(), "USER"));
        } catch (Exception e) {
            SecurityContext.clear();
        }
    }
}

