package org.example.framework.security.middleware;

import org.example.framework.security.request.Request;

import java.net.Socket;

public interface Middleware {
    void handle(final Socket socket, final Request request);
}

