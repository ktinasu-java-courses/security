package org.example.framework.security.proxy;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Auth {
    private final String name;
    private final String role;
}

