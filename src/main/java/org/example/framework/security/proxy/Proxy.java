package org.example.framework.security.proxy;


import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.SecurityContext;

import java.nio.file.AccessDeniedException;

@Slf4j
public class Proxy {
    public Object securedProxy(Object object, Class<?> originalClazz, Class<?>[] argumentTypes, Object[] arguments) {
        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(originalClazz);
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
            if (!method.isAnnotationPresent(HasRole.class)) {
                return method.invoke(object, args);
            }

            log.debug("method secured: {}", method.getName());
            final HasRole annotation = method.getAnnotation(HasRole.class);
            final String role = annotation.value();

            final Auth auth = SecurityContext.getAuthRole();
            if (!auth.getRole().equals(role)) {
                log.warn("access denied for {} to method {}", auth, method.getName());
                throw new AccessDeniedException("access denied");
            }

            log.debug("access allowed for {} to method {}", auth, method.getName());
            return method.invoke(object, args);
        });

        return enhancer.create(argumentTypes, arguments);
    }

    public Object auditedProxy(Object object, Class<?> originalClazz, Class<?>[] argumentTypes, Object[] arguments) {
        final Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(originalClazz);
        enhancer.setCallback((MethodInterceptor) (obj, method, args, proxy) -> {
            if (method.isAnnotationPresent(Audit.class)) {
                log.debug("method invoked: {}", method.getName());
            }
            return method.invoke(object, args);
        });
        return enhancer.create(argumentTypes, arguments);
    }
}
