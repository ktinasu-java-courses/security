package org.example.framework.security.request;

public interface Request {
    String getMethod();

    String getPath();

    String getQuery();

    String getHttpVersion();

    java.util.Map<String, String> getHeaders();

    byte[] getBody();
}

